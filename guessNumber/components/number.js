import React from 'react'
import {View,Text,StyleSheet} from 'react-native'

const Number = (props)=>{
    return(
        <View style={styles.container}>
            <Text style={styles.text}>{props.children}</Text>
        </View>
    )
}
const styles=StyleSheet.create({
    container:{
        marginVertical:5,
        borderWidth:3,
        borderColor:'red',
        padding:10,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:10
    },
    text:{
        fontSize:20,

    }
})
export default Number