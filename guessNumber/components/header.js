import React from "react";
import { View, Text, StyleSheet } from "react-native";

const Header = (props) => {
  return (
    <View style={styles.header}>
      <Text style={styles.headerText}>{props.title}</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  header: {
    padding: 20,
    width: "100%",
    alignItems: "center",
    backgroundColor: "green",
  },
  headerText: {
    fontSize: 20,
    marginTop:3
  },
});
export default Header;
