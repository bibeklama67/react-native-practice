import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import Header from "./components/header";
import StartGame from "./screens/startNewGame";
import GameScreen from "./screens/gameScreen";
import GameOverScreen from "./screens/gameOver";
import * as Font from "expo-font";
import { AppLoading } from "expo";

const fetchFonts = () => {
  return Font.loadAsync({
    "open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
    "open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf"),
  });
};

export default function App() {
  const [selectedNumber, setSelectedNumber] = useState();
  const [guessRounds, setRounds] = useState(0);
  const [dataLoaded, setDataLoaded] = useState(false);

  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setDataLoaded(true)}
        onError={(err) => console.log(err)}
      />
    );
  }
  const configureNewGame = () => {
    setSelectedNumber(null);
    setRounds(0);
  };
  const numberSelector = (number) => {
    setSelectedNumber(number);
    setRounds(0);
  };

  const gameOverHandler = (numOfRounds) => {
    setRounds(numOfRounds);
  };

  let content = <StartGame numberSelector={numberSelector} />;
  if (selectedNumber && guessRounds <= 0) {
    content = (
      <GameScreen userChoice={selectedNumber} onGameOver={gameOverHandler} />
    );
  } else if (guessRounds > 0) {
    content = (
      <GameOverScreen
        number={selectedNumber}
        rounds={guessRounds}
        configureNewGame={configureNewGame}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Header title="GuessNumber" />
      {content}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
