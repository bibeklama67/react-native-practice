import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  Button,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from "react-native";
import Card from "../components/card";
import color from "../components/colors";
import Input from "../components/input";
import Number from "../components/number";
import DefaultStyles from "../constants/default-styles";
import MainButton from '../components/MainButton'
const StartGame = (props) => {
  const [enterNumber, setNumber] = useState("");
  const [confirmNumber, setConfirmNumber] = useState();
  const [confirmed, setConfirmed] = useState(false);
  const inputHandler = (inputText) => {
    setNumber(inputText);
  };
  const resetHandler = () => {
    setNumber(""), setConfirmed(false);
  };
  const setConfirmNumberHandler = () => {
    Keyboard.dismiss();
    const choosenNumber = parseInt(enterNumber);
    if (isNaN(choosenNumber) || choosenNumber <= 0 || choosenNumber > 99) {
      Alert.alert(
        "Invalid Number",
        "NUmber must be grater and smaller than 100",
        [
          {
            text: "OK",
            style: "destructive",
            onPress: resetHandler,
          },
        ]
      );
    }
    setNumber("");
    setConfirmNumber(choosenNumber);
    setConfirmed(true);
  };
  let output;
  if (confirmed) {
    output = (
      <Card style={styles.startGame}>
        <Text>Your Number is : </Text>
        <Number>{confirmNumber}</Number>
        <MainButton
         
          onPress={() => props.numberSelector(confirmNumber)}
        >STARTS</MainButton>
      </Card>
    );
  }
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <View>
        <Card>
          <View style={styles.mainTitle}>
            <Text style={DefaultStyles.bodyTitle} >Start New Game</Text>
          </View>
          <Card style={styles.startGameContainer}>
            <Text style={styles.title}>Select The Number</Text>
            <Input
              style={styles.input}
              blurOnSubmit
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="number-pad"
              maxLength={2}
              onChangeText={inputHandler}
              value={enterNumber}
            />
            <View style={styles.buttonContainer}>
              <View style={styles.button}>
                <Button
                  onPress={setConfirmNumberHandler}
                  title="Confirm"
                  color={color.secondary}
                ></Button>
              </View>
              <View style={styles.button}>
                <Button
                  onPress={resetHandler}
                  title="Reset"
                  color={color.primary}
                ></Button>
              </View>
            </View>
          </Card>
        </Card>
        {output}
      </View>
    </TouchableWithoutFeedback>
  );
};
const styles = StyleSheet.create({
  startGame: {
    alignItems: "center",
  },
  input: {
    width: 50,
  },
  mainTitle: {
    alignItems: "center",
  },
  titleHeader: {
    fontSize: 25,
    margin: 5,
  },
  startGameContainer: {
    width: 300,
    maxWidth: "100%",
    alignItems: "center",
  },
  buttonContainer: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    padding: 20,
  },
  title: {
    fontFamily: "open-sans",
    fontSize: 15,
    marginVertical: 5,
  },
});
export default StartGame;
