import React from "react";
import { View, Text, StyleSheet, Button, Image } from "react-native";
import MainButton from '../components/MainButton'
const GameOverScreen = (props) => {
  return (
    <View style={styles.screen}>
      <Text>The Game is Over</Text>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          //source={{uri:'https://lkjafkldsjfakl;jdfkjasdf'}}
          source={require("../assets/success.png")}
        />
      </View>
      <Text>
        Number is : <Text style={styles.textDynamic}>{props.number}</Text>
      </Text>
      <Text>
        Number of Rounds :
        <Text style={styles.textDynamic}> {props.rounds}</Text>
      </Text>
      <MainButton onPress={props.configureNewGame} >NEW GAME</MainButton>
    </View>
  );
};
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  imageContainer: {
    width: 300,
    height: 300,
    borderRadius: 150,
    borderWidth: 3,
    borderColor: "black",
    overflow: "hidden",
  },
  image: { width: "100%", height: "100%" },
  textDynamic: {
    color: "red",
  },
});
export default GameOverScreen;
