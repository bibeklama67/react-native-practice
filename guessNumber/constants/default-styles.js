import { StyleSheet } from "react-native";
export default StyleSheet.create({
  bodyTitle: {
    fontFamily: "open-sans-bold",
    fontSize:25
  },
});
