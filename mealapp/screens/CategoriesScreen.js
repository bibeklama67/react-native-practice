import React from "react";
import {
  Text,
  View,
  StyleSheet,
  Button,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { CATEGORIES } from "../data/data";

const CategoriesScreen = (props) => {
  const renderGridItem = (itemdata) => {
    return (
      <TouchableOpacity
        style={styles.girdItems}
        onPress={() => {
          props.navigation.navigate("meal", { categoryId: itemdata.item.id });
        }}
      >
        <View
          style={{
            backgroundColor: itemdata.item.color,
            flex: 1,
            borderRadius: 10,
            shadowColor: "black",
            shadowOpacity: 0.26,
            shadowOffset: { width: 0, height: 2 },
            elevation: 3,
            padding: 15,
            justifyContent: "flex-end",
            alignItems: "flex-end",
          }}
        >
          <Text style={styles.title}>{itemdata.item.title}</Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <FlatList
      keyExtractor={(item, index) => item.id}
      data={CATEGORIES}
      renderItem={renderGridItem}
      numColumns={2}
    />
  );
};
CategoriesScreen.navigationOptions = {
  headerTitle: "Meal Categories",
  headerStyle: {
    backgroundColor: "green",
  },
  headerTintColor: "white",
};
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  girdItems: {
    flex: 1,
    margin: 20,
    height: 150,
  },
  title:{
    fontFamily:'open-sans-bold'
  }
});
export default CategoriesScreen;
