import CategoriesScreen from "../screens/CategoriesScreen";
import * as React from "react";
import CategoryMealScreen from "../screens/CategoryMealScreen";
import MealDetailsScreen from "../screens/MealDetailsScreen";
import FavouriteScreen from "../screens/FavouriteScreen";
import { Ionicons } from "@expo/vector-icons";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { StyleSheet } from "react-native";

const Stack = createStackNavigator();
function MealsNavigations() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Meal Categories"
        component={CategoriesScreen}
        options={CategoriesScreen.navigationOptions}
      />
      <Stack.Screen
        name="meal"
        component={CategoryMealScreen}
        options={CategoryMealScreen.navigationOptions}
      />
      <Stack.Screen
        name="details"
        component={MealDetailsScreen}
        options={MealDetailsScreen.navigationOptions}
      />
    </Stack.Navigator>
  );
}
const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      style={styles.tab}
      tabBarOptions={{
        activeTintColor: "#e91e63",
      }}
    >
      <Tab.Screen
        name="Home"
        component={MealsNavigations}
        options={{
          tabBarIcon: ({ color }) => {
            return <Ionicons name="ios-restaurant" size={25} color={color} />;
          },
        }}
      />
      <Tab.Screen
        name="Favourites"
        component={FavouriteScreen}
        options={{
          tabBarIcon: ({ color }) => {
            return <Ionicons name="ios-star" size={25} color={color} />;
          },
        }}
      />
    </Tab.Navigator>
  );
}
const styles = StyleSheet.create({
  tab: {
    backgroundColor: "red",
  },
});
export default MyTabs;
