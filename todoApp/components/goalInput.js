import React, { useState } from "react";
import { StyleSheet, View, TextInput, Button, Modal } from "react-native";

const GoalInput = (props) => {
  const [text, setText] = useState("");
  const inputChangeHandler = (enterdText) => {
    setText(enterdText);
  };
  const goalInptHandler = () => {
    props.addGoalHandler(text);
    setText("");
  };
  return (
    <Modal visible={props.modalShow} animationType="slide">
      <View style={styles.top}>
        <TextInput
          style={styles.inputField}
          placeholder="input goal"
          onChangeText={inputChangeHandler}
          value={text}
        ></TextInput>
        <View style={styles.buttonGroup}>
          <Button title="ADD" onPress={goalInptHandler} />
          <Button title="CANCEL" onPress={props.cancelModalHandler} />
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  inputField: {
    padding: 10,
    width: "80%",
    borderBottomColor: "black",
    borderWidth: 1,
    borderTopColor: "white",
    borderLeftColor: "white",
    borderRightColor: "white",
    marginBottom: 30,
  },
  top: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonGroup: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "50%",
  },
});
export default GoalInput;
