import React from "react";
import { View, Text ,StyleSheet,TouchableOpacity} from "react-native";

const GoalItem = (props) => {
  return (
    <TouchableOpacity onPress={props.deleteHandler.bind(this,props.id)}>
      <View style={styles.listItem} key={Math.random().toString}>
      <Text>{props.item}</Text>
    </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
    listItem: {
        padding: 10,
        borderColor: "gray",
        borderWidth: 2,
        backgroundColor: "gray",
        marginVertical: 2,
      }
});
export default GoalItem;
