import React, { useState } from "react";
import { StyleSheet, View, Button, FlatList } from "react-native";
import GoalItem from "./components/goalItem";
import GoalInput from "./components/goalInput";

export default function App() {
  const [goal, setGoal] = useState([]);
  const [addModal, setAddModal] = useState(false);
  const addGoalHandler = (text) => {
    setGoal((currentGoal) => [
      ...currentGoal,
      {
        id: Math.random().toString(),
        value: text,
      },
    ]);
    setAddModal(false);
  };
  const removeGoalHandler = () => {
    setGoal([]);
  };
  const deleteHandler = (id) => {
    setGoal((goal) => {
      return goal.filter((goal) => goal.id !== id);
    });
  };
  const cancelModalHandler = () => {
    setAddModal(false);
  };
  return (
    <View style={styles.main}>
      <View style={styles.buttonAdd}>
        <Button title="ADD NEW GOAL" onPress={() => setAddModal(true)} />
      </View>
      <GoalInput
        modalShow={addModal}
        addGoalHandler={addGoalHandler}
        cancelModalHandler={cancelModalHandler}
      />
      <FlatList
        keyExtractor={(item, index) => item.id}
        data={goal}
        renderItem={(itemData) => (
          <GoalItem
            id={itemData.item.id}
            deleteHandler={deleteHandler}
            item={itemData.item.value}
          />
        )}
      />
      <View>
        <Button title="Clear" onPress={removeGoalHandler} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    padding: 40,
  },
  buttonAdd: {
    backgroundColor: "black",
  },
});
